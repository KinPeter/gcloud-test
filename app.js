const express = require('express');
const app = express();

const env = process.env.MY_ENV || "local";
const env2 = process.env.SEC_ENV || "Lajos";

app.get('/', (req, res) => {
    res.send(`Hello from my ${env} App Engine! \n My name is ${env2}`);
});

// Listen to the App Engine-specified port, or 8080 otherwise
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}...`);
});